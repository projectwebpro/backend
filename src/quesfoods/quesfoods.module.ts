import { Module } from '@nestjs/common';
import { QuesfoodsService } from './quesfoods.service';
import { QuesfoodsController } from './quesfoods.controller';
import { Quesfood } from './entities/quesfood.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderItem } from 'src/orders/entities/order-item';
import { Food } from 'src/foods/entities/food.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Quesfood, OrderItem, Food])],
  controllers: [QuesfoodsController],
  providers: [QuesfoodsService],
})
export class QuesfoodsModule {}
