import { Test, TestingModule } from '@nestjs/testing';
import { QuesfoodsController } from './quesfoods.controller';
import { QuesfoodsService } from './quesfoods.service';

describe('QuesfoodsController', () => {
  let controller: QuesfoodsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [QuesfoodsController],
      providers: [QuesfoodsService],
    }).compile();

    controller = module.get<QuesfoodsController>(QuesfoodsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
