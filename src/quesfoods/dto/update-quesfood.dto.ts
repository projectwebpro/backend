import { PartialType } from '@nestjs/mapped-types';
import { CreateQuesfoodDto } from './create-quesfood.dto';

export class UpdateQuesfoodDto extends PartialType(CreateQuesfoodDto) {}
