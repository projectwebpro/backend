import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Food } from 'src/foods/entities/food.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { Repository } from 'typeorm';
import { CreateQuesfoodDto } from './dto/create-quesfood.dto';
import { UpdateQuesfoodDto } from './dto/update-quesfood.dto';
import { Quesfood } from './entities/quesfood.entity';

@Injectable()
export class QuesfoodsService {
  constructor(
    @InjectRepository(Quesfood)
    private quesfoodRepository: Repository<Quesfood>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
  ) {}

  async create(createQuesfoodDto: CreateQuesfoodDto) {
    // console.log(createQuesfoodDto);
    // const quefood: Quesfood = new Quesfood();
    // for (const oditem of createQuesfoodDto.orderItems) {
    //   const orderItem = new OrderItem();
    //   orderItem.amount = oditem.amount;
    //   orderItem.food = await this.foodsRepository.findOneBy({
    //     id: oditem.foodId,
    //   });
    //   orderItem.name = orderItem.food.name;
    //   //โต๊ะยังไม่มี สถานะยังไม่มี
    //   //  orderItem.quefood = quefood; // อ้างกลับ
    //   await this.quesfoodRepository.save(orderItem);
    //   quefood.amount = 1;
    //   quefood.namefood = orderItem.food.name;
    //   quefood.status = 'รอเตรียม';
    //   quefood.table = 4;
    // }

    // return await this.quesfoodRepository.findOne({
    //   where: { id: quefood.id },
    //   relations: ['orderItems'],
    // });

    return this.quesfoodRepository.save(createQuesfoodDto);
  }

  findAll() {
    return this.quesfoodRepository.find({
      relations: ['orderItems', 'orderItems.order', 'orderItems.order.table'],
    });
  }
  findStatusQue(status: string) {
    return this.quesfoodRepository.find({
      relations: ['orderItems', 'orderItems.order', 'orderItems.order.table'],
      where: { status: status },
    });
  }

  findQueByOrderId(orderItemId: number) {
    return this.quesfoodRepository.find({
      relations: ['orderItems', 'orderItems.order', 'orderItems.order.table'],
      where: { orderItemId: orderItemId },
    });
  }

  async findOne(id: number) {
    const quesfood = await this.quesfoodRepository.findOne({
      where: { id: id },
    });
    if (!quesfood) {
      throw new NotFoundException();
    }
    return quesfood;
  }

  async update(id: number, updateQuesfoodDto: UpdateQuesfoodDto) {
    const quesfood = await this.quesfoodRepository.findOneBy({ id: id });
    if (!quesfood) {
      throw new NotFoundException();
    }
    const updatedQuefood = { ...quesfood, ...updateQuesfoodDto };
    return this.quesfoodRepository.save(updatedQuefood);
  }

  async remove(id: number) {
    const quesfood = await this.quesfoodRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedQuefoos = await this.quesfoodRepository.softRemove(quesfood);
      return deletedQuefoos;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
