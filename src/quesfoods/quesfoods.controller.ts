import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { QuesfoodsService } from './quesfoods.service';
import { CreateQuesfoodDto } from './dto/create-quesfood.dto';
import { UpdateQuesfoodDto } from './dto/update-quesfood.dto';

@Controller('quesfoods')
export class QuesfoodsController {
  constructor(private readonly quesfoodsService: QuesfoodsService) {}

  @Post()
  create(@Body() createQuesfoodDto: CreateQuesfoodDto) {
    return this.quesfoodsService.create(createQuesfoodDto);
  }

  @Get()
  findAll() {
    return this.quesfoodsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.quesfoodsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateQuesfoodDto: UpdateQuesfoodDto,
  ) {
    return this.quesfoodsService.update(+id, updateQuesfoodDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.quesfoodsService.remove(+id);
  }

  @Get('status/:text')
  findAllByStatus(@Param('text') status: string) {
    return this.quesfoodsService.findStatusQue(status);
  }

  // @Get('order/:id')
  // findQueByOrderId(@Param('id') id: string) {
  //   return this.quesfoodsService.findQueByOrderId(+id);
  // }

  @Get('orderItem/:id')
  findQueByOrderId(@Param('id') id: string) {
    return this.quesfoodsService.findQueByOrderId(+id);
  }
}
