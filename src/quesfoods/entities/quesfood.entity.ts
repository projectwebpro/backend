import { OrderItem } from 'src/orders/entities/order-item';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
@Entity()
export class Quesfood {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  status: string;
  @ManyToOne(() => OrderItem, (orderItem) => orderItem.quefood)
  orderItems: OrderItem;
  @Column()
  orderItemId: number;
  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
}
