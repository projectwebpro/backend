import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '30',
  })
  name: string;

  @Column({
    length: '30',
  })
  position: string;

  @Column()
  work_date: string;

  @Column()
  work_in: string;

  @Column()
  work_out: string;

  @Column()
  work_hour: number;

  @Column()
  salary: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
