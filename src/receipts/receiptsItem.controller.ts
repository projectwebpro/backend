import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';

import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { ReceiptsItemsService } from './receiptsItem.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@UseGuards(JwtAuthGuard) // ต้องใส่ทุกตัว หรือไม่ใส่ก็ได้
@Controller('receiptsItems')
export class ReceiptsItemsController {
  constructor(private readonly receiptsItemsService: ReceiptsItemsService) {}

  // @Post()
  // create(@Body() createReceiptDto: CreateReceiptDto) {
  //   return this.receiptsService.create(createReceiptDto);
  // }

  @Get()
  findAll() {
    return this.receiptsItemsService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.receiptsService.findOne(+id);
  // }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateReceiptItemDto: UpdateReceiptDto,
  ) {
    return this.receiptsItemsService.update(+id, updateReceiptItemDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.receiptsItemsService.remove(+id);
  }
}
