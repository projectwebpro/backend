import { Test, TestingModule } from '@nestjs/testing';
import { ReceiptsItemsService } from './receiptsItem.service';

describe('ReceiptsItemsService', () => {
  let service: ReceiptsItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReceiptsItemsService],
    }).compile();

    service = module.get<ReceiptsItemsService>(ReceiptsItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
