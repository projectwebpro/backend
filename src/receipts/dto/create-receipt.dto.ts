import { IsPositive, IsEmpty, IsNotEmpty } from 'class-validator';
export class CreateReceiptDto {
  @IsNotEmpty()
  total: number; //เงินรวม
  @IsNotEmpty()
  receive: number; //เงินรับ
  @IsNotEmpty()
  change: number; // เงินทอน
  @IsNotEmpty()
  nameEmp: string; // ชื่อพนักงาน
  @IsNotEmpty()
  receiptItems: CreatedReceiptItemDto[];
}

export class CreatedReceiptItemDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  amount: number;
  @IsNotEmpty()
  price: number;
  @IsNotEmpty()
  total: number;
  @IsNotEmpty()
  receiptId: number;
  @IsNotEmpty()
  materialId: number;
}
