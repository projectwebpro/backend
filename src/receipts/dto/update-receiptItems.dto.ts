import { PartialType } from '@nestjs/mapped-types';
import { CreateReceiptDto, CreatedReceiptItemDto } from './create-receipt.dto';

export class UpdateReceiptItemDto extends PartialType(CreatedReceiptItemDto) {}
