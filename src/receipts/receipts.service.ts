import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { ReceiptItem } from './entities/receipt-items';
import { Receipt } from './entities/receipt.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptRepository: Repository<Receipt>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(ReceiptItem)
    private receiptItemRepository: Repository<ReceiptItem>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto) {
    // return this.receiptRepository.save(createReceiptDto);
    // console.log(createReceiptDto);
    const receipt: Receipt = new Receipt();
    //เริ่มต้น
    receipt.nameEmp = createReceiptDto.nameEmp;
    receipt.total = createReceiptDto.total;
    receipt.receive = createReceiptDto.receive; // จ่าย
    receipt.change = createReceiptDto.change;

    await this.receiptRepository.save(receipt); // ได้ id receipt

    // const material: Material = new Material();
    // material.id=
    for (const re of createReceiptDto.receiptItems) {
      const receiptItem = new ReceiptItem();
      receiptItem.name = re.name;
      receiptItem.amount = re.amount;
      receiptItem.price = re.price;
      receiptItem.total = re.total;
      receiptItem.material = await this.materialRepository.findOneBy({
        id: re.materialId,
        // name: re.name,
      }); //FK //อยู่ตรงนี้
      receiptItem.receipt = await this.receiptRepository.findOneBy({
        id: re.receiptId,
      }); //FK
      receiptItem.receipt = receipt; // อ้างกลับ receipt
      // receiptItem.material=m
      await this.receiptItemRepository.save(receiptItem);
      //console.log(receiptItem.name);
    }

    await this.receiptRepository.save(receipt);
    return this.receiptRepository.findOne({
      where: { id: receipt.id },
      relations: ['receiptItems'],
    });
  }

  findAll() {
    return this.receiptRepository.find({ relations: ['receiptItems'] });
  }

  findOne(id: number) {
    return this.receiptRepository.findOne({
      where: { id: id },
      relations: ['receiptItems', 'material'],
    });
  }

  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} receipt`;
  }

  async remove(id: number) {
    const receipt = await this.receiptRepository.findOneBy({ id: id });
    return this.receiptRepository.softRemove(receipt);
  }
}
