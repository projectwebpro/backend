import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ReceiptItem } from './receipt-items';
@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: 'double' })
  total: number; //เงินรวม
  @Column({ type: 'double' })
  receive: number; //เงินรับ
  @Column({ type: 'double' })
  change: number; // เงินทอน
  @Column()
  nameEmp: string; // ชื่อพนักงาน
  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updateDate: Date;
  @DeleteDateColumn()
  deleteDate: Date;
  @OneToMany(() => ReceiptItem, (receiptItems) => receiptItems.receipt)
  receiptItems: ReceiptItem[];
}
