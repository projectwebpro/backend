import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  create(createMaterialDto: CreateMaterialDto) {
    return this.materialsRepository.save(createMaterialDto);
  }

  findAll() {
    return this.materialsRepository.find();
  }

  findOne(id: number) {
    return this.materialsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    try {
      const updatedMaterial = await this.materialsRepository.save({
        id,
        ...updateMaterialDto,
      });
      return updatedMaterial;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number) {
    const material = await this.materialsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedMaterial = await this.materialsRepository.softRemove(
        material,
      );
      return deletedMaterial;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
