// import { StockItem } from 'src/stocks/entities/stock-items';
import { ReceiptItem } from 'src/receipts/entities/receipt-items';
import { StockItem } from 'src/stocks/entities/stock-items';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  UpdateDateColumn,
  CreateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    length: '32',
  })
  name: string;
  @Column()
  minAmount: number;
  @Column()
  unit: string;
  @Column()
  balance: number;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => StockItem, (stockItems) => stockItems.stock)
  stockItems: StockItem[];
  @OneToMany(() => ReceiptItem, (receiptItems) => receiptItems.receipt)
  receiptItems: ReceiptItem[];
}
