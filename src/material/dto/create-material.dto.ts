import { IsNotEmpty } from 'class-validator';
export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  minAmount: number;
  @IsNotEmpty()
  unit: string;
  @IsNotEmpty()
  balance: number;
}
