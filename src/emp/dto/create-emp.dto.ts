import { IsNotEmpty, Length, MaxLength } from 'class-validator';

export class CreateEmpDto {
  @IsNotEmpty()
  @Length(3, 30)
  name: string;

  @IsNotEmpty()
  gender: string;

  @IsNotEmpty()
  position: string;

  @IsNotEmpty()
  @MaxLength(10)
  tel: string;
}
