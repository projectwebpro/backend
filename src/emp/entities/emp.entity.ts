import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Emp {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '30',
  })
  name: string;

  @Column()
  gender: string;

  @Column()
  position: string;

  @Column()
  tel: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
