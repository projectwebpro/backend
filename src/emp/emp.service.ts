import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmpDto } from './dto/create-emp.dto';
import { UpdateEmpDto } from './dto/update-emp.dto';
import { Emp } from './entities/emp.entity';

@Injectable()
export class EmpService {
  constructor(
    @InjectRepository(Emp)
    private empRepository: Repository<Emp>,
  ) {}

  create(createEmpDto: CreateEmpDto) {
    return this.empRepository.save(createEmpDto);
  }

  findAll() {
    return this.empRepository.find();
  }

  findOne(id: number) {
    return this.empRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateEmpDto: UpdateEmpDto) {
    try {
      const updatedEmp = await this.empRepository.save({
        id,
        ...updateEmpDto,
      });
      return updatedEmp;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const emp = await this.empRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedEmp = await this.empRepository.remove(emp);
      return deletedEmp;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
