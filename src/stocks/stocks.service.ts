import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { StockItem } from './entities/stock-items';
import { Stock } from './entities/stock.entity';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private stockRepository: Repository<Stock>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(StockItem)
    private stockItemRepository: Repository<StockItem>,
  ) {}

  async create(createStockDto: CreateStockDto) {
    // // save stock
    // console.log(createStockDto);
    // const stock: Stock = new Stock();
    // //เริ่มต้น
    // stock.nameEmp = 'สพลดัย';
    // await this.stockRepository.save(stock); // id

    // for (const st of createStockDto.stockItem) {
    //   const stockItem = new StockItem();
    //   stockItem.amount = st.amount;
    //   stockItem.material = await this.materialRepository.findOneBy({
    //     id: st.MaterialId,
    //   }); //FK
    //   stockItem.nameMaterial = stockItem.material.name; // ชื่อ material
    //   stockItem.stock = await this.stockRepository.findOneBy({
    //     id: st.StockId,
    //   }); //FK
    //   stockItem.stock = stock; // อ้างกลับ
    //   await this.stockItemRepository.save(stockItem);
    //   stock.nameEmp = stock.nameEmp;
    // }
    // await this.stockRepository.save(stock);
    // console.log(await this.stockRepository.save(stock));
    // return this.stockRepository.findOne({
    //   where: { id: stock.id },
    //   relations: ['stockItem'],
    // }); //or return stock
    return this.stockRepository.save(createStockDto);
  }

  findAll() {
    return this.stockRepository.find({ relations: ['stockItem'] });
  }

  findOne(id: number) {
    return this.stockRepository.findOne({
      where: { id: id },
      relations: ['stockItems'],
    });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    try {
      const updateStocksDto = await this.stockRepository.save({
        id,
        ...updateStockDto,
      });
      return updateStockDto;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const stock = await this.stockRepository.findOneBy({ id: id });
    return this.stockRepository.softRemove(stock);
  }
}
