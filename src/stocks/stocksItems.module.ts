import { Module } from '@nestjs/common';
import { StocksService } from './stocks.service';
import { StocksController } from './stocks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { Material } from 'src/material/entities/material.entity';
import { StockItem } from './entities/stock-items';
import { StocksItemsController } from './stocksItems.controller';
import { StocksItemsService } from './stocksItems.service';

@Module({
  imports: [TypeOrmModule.forFeature([Stock, StockItem, Material])],
  controllers: [StocksItemsController],
  providers: [StocksItemsService],
})
export class StockItemsModule {}
