import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreateStockDto, CreateStockItemDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { StockItem } from './entities/stock-items';
import { Stock } from './entities/stock.entity';
import { UpdateStockItemsDto } from './dto/update-stockItem.dto';

@Injectable()
export class StocksItemsService {
  constructor(
    @InjectRepository(Stock)
    private stockRepository: Repository<Stock>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(StockItem)
    private stockItemRepository: Repository<StockItem>,
  ) {}

  async create(createStockItemsDto: CreateStockItemDto) {
    return this.stockItemRepository.save(createStockItemsDto);
  }

  findAll() {
    return this.stockItemRepository.find();
  }
  // findOne(id: number) {
  //   return this.stockRepository.findOne({
  //     where: { id: id },
  //     relations: ['stockItems'],
  //   });
  // }

  async update(id: number, updateStockItemsDto: UpdateStockItemsDto) {
    try {
      const updatedStockItem = await this.stockItemRepository.save({
        id,
        ...updateStockItemsDto,
      });
      return updateStockItemsDto;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const stockItem = await this.stockItemRepository.findOneBy({ id: id });
    return this.stockItemRepository.softRemove(stockItem);
  }
}
