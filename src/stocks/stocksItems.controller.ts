import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { StocksService } from './stocks.service';
import { CreateStockDto, CreateStockItemDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { StocksItemsService } from './stocksItems.service';
import { UpdateStockItemsDto } from './dto/update-stockItem.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
@UseGuards(JwtAuthGuard) // ต้องใส่ทุกตัว หรือไม่ใส่ก็ได้
@Controller('stocksItems')
export class StocksItemsController {
  constructor(private readonly stocksItemsService: StocksItemsService) {}

  @Post()
  create(@Body() createItemsStockDto: CreateStockItemDto) {
    return this.stocksItemsService.create(createItemsStockDto);
  }

  @Get()
  findAll() {
    return this.stocksItemsService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.stocksService.findOne(+id);
  // }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStockItemsDto: UpdateStockItemsDto,
  ) {
    return this.stocksItemsService.update(+id, updateStockItemsDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stocksItemsService.remove(+id);
  }
}
