import { Test, TestingModule } from '@nestjs/testing';
import { StocksController } from './stocks.controller';
import { StocksService } from './stocks.service';
import { StocksItemsController } from './stocksItems.controller';
import { StocksItemsService } from './stocksItems.service';

describe('StocksItemsController', () => {
  let controller: StocksItemsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StocksController],
      providers: [StocksItemsService],
    }).compile();

    controller = module.get<StocksItemsController>(StocksItemsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
