import { Material } from 'src/material/entities/material.entity';
import { Stock } from './stock.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class StockItem {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  nameMaterial: string;
  @Column()
  amount: number;
  @Column({ type: 'double' })
  price: number;
  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updateDate: Date;
  @DeleteDateColumn()
  deleteDate: Date;
  @ManyToOne(() => Stock, (stock) => stock.stockItem)
  stock: Stock;
  @ManyToOne(() => Material, (material) => material.stockItems)
  material: Material;
}
