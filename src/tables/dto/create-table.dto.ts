import { IsNotEmpty, Length } from 'class-validator';

export class CreateTableDto {
  @IsNotEmpty()
  table: string;

  @IsNotEmpty()
  seat: number;

  @IsNotEmpty()
  status: string;
}
