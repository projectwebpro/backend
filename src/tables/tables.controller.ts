import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { TablesService } from './tables.service';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('tables')
export class TablesController {
  constructor(private readonly tablesService: TablesService) {}
  @UseGuards(JwtAuthGuard) // ต้องใส่ทุกตัว หรือไม่ใส่ก็ได้
  @Post()
  create(@Body() createTableDto: CreateTableDto) {
    console.log(createTableDto);
    return this.tablesService.create(createTableDto);
  }

  @Get()
  findAll() {
    return this.tablesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.tablesService.findOne(+id);
  }
  @UseGuards(JwtAuthGuard) // ต้องใส่ทุกตัว หรือไม่ใส่ก็ได้
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTableDto: UpdateTableDto) {
    return this.tablesService.update(+id, updateTableDto);
  }
  @UseGuards(JwtAuthGuard) // ต้องใส่ทุกตัว หรือไม่ใส่ก็ได้
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.tablesService.remove(+id);
  }

  @Get('status/:text')
  findStatusTable(@Param('text') status: string) {
    return this.tablesService.findStatusTable(status);
  }
}
