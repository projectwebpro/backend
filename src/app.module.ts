import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Food } from './foods/entities/food.entity';
import { FoodsModule } from './foods/foods.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { TablesModule } from './tables/tables.module';
import { Table } from './tables/entities/table.entity';
// import { StocksModule } from './stocks/stocks.module';
// import { Stock } from './stocks/entities/stock.entity';
// import { StockItem } from './stocks/entities/stock-items';
import { Material } from './material/entities/material.entity';
import { MaterialModule } from './material/material.module';
import { Stock } from './stocks/entities/stock.entity';
import { StocksModule } from './stocks/stocks.module';
import { StockItem } from './stocks/entities/stock-items';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { ReceiptItem } from './receipts/entities/receipt-items';
import { OrdersModule } from './orders/orders.module';
import { Customer } from './customers/entities/customer.entity';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { CustomersModule } from './customers/customers.module';
import { QuesfoodsModule } from './quesfoods/quesfoods.module';
import { Quesfood } from './quesfoods/entities/quesfood.entity';
import { EmpModule } from './emp/emp.module';
import { Emp } from './emp/entities/emp.entity';
import { CategoriesModule } from './categories/categories.module';
import { Category } from './categories/entities/category.entity';
import { AuthModule } from './auth/auth.module';
import { SalaryModule } from './salary/salary.module';
import { Salary } from './salary/entities/salary.entity';
import { StockItemsModule } from './stocks/stocksItems.module';
import { ReceiptsItemsModule } from './receipts/receiptsItem.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',
      entities: [
        Food,
        User,
        Table,
        Material,
        Stock,
        StockItem,
        Receipt,
        ReceiptItem,
        Customer,
        Order,
        OrderItem,
        Quesfood,
        Emp,
        Category,
        Salary,
      ], //ต้องเพิ่ม Entity ทั้งหมดที่เรามี
      synchronize: true,
    }),
    TablesModule,
    StocksModule,
    FoodsModule,
    UsersModule,
    MaterialModule,
    ReceiptsModule,
    OrdersModule,
    CustomersModule,
    QuesfoodsModule,
    EmpModule,
    CategoriesModule,
    AuthModule,
    SalaryModule,
    StockItemsModule,
    ReceiptsItemsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
