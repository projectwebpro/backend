import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateFoodDto } from './dto/create-food.dto';
import { UpdateFoodDto } from './dto/update-food.dto';
import { Food } from './entities/food.entity';

@Injectable()
export class FoodsService {
  constructor(
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}

  async create(createFoodDto: CreateFoodDto) {
    const category = await this.categoriesRepository.findOne({
      where: {
        id: createFoodDto.categoryId,
      },
    });
    const newFood = new Food();
    newFood.name = createFoodDto.name;
    newFood.price = createFoodDto.price;
    newFood.image = createFoodDto.image;
    newFood.category = category;
    return this.foodsRepository.save(newFood);
  }

  findByCategory(id: number) {
    return this.foodsRepository.find({ where: { categoryId: id } });
  }

  findAll(option) {
    return this.foodsRepository.find(option);
  }

  findOne(id: number) {
    return this.foodsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateFoodDto: UpdateFoodDto) {
    try {
      const updatedFood = await this.foodsRepository.save({
        id,
        ...updateFoodDto,
      });
      return updatedFood;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const food = await this.foodsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedFood = await this.foodsRepository.softRemove(food);
      return deletedFood;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
