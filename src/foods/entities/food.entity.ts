import { Category } from 'src/categories/entities/category.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Food {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ type: 'float' })
  price: number;

  @Column({
    length: '128',
    default: 'no_img_available.jpg',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.food)
  orderItems: OrderItem[];

  @ManyToOne(() => Category, (category) => category.foods)
  category: Category;

  @Column()
  categoryId: number;
}
