import { IsNotEmpty, Length } from 'class-validator';

export class CreateFoodDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  price: number;

  image = 'no_img_available.jpg';

  @IsNotEmpty()
  categoryId: number;
}
