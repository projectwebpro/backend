import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';

import { Food } from 'src/foods/entities/food.entity';
import { Table } from 'src/tables/entities/table.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const customer = await this.customersRepository.findOneBy({
      id: createOrderDto.customerId,
    });

    const table = await this.tablesRepository.findOneBy({
      id: createOrderDto.tableId,
    });

    const order: Order = new Order();
    order.customer = customer;
    order.table = table;
    order.amount = 0;
    order.total = 0;
    await this.ordersRepository.save(order); // ได้ id

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.food = await this.foodsRepository.findOneBy({
        id: od.foodId,
      }); //id food
      orderItem.name = orderItem.food.name;
      orderItem.price = orderItem.food.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order; // อ้างกลับ
      await this.orderItemsRepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }
    await this.ordersRepository.save(order); // ได้ id
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['customer', 'orderItems', 'table'],
    });
  }

  // findAllItem() {
  //   return this.orderItemsRepository.find({
  //     relations: ['customer', 'order'],
  //   });
  // }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['table', 'customer', 'orderItems'],
    });
  }

  findOrderTableId(tableId: number) {
    return this.ordersRepository.find({
      where: { tableId: tableId },
      relations: ['orderItems'],
    });
  }

  findOrderItemId(orderId: number) {
    return this.orderItemsRepository.find({
      where: { id: orderId },
      relations: ['table', 'order'],
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    try {
      const updatedOrder = await this.ordersRepository.save({
        id,
        ...updateOrderDto,
      });

      for (const od of updateOrderDto.orderItems) {
        const orderItem = new OrderItem();
        orderItem.amount = od.amount;
        orderItem.food = await this.foodsRepository.findOneBy({
          id: od.foodId,
        });
        orderItem.name = orderItem.food.name;
        orderItem.price = orderItem.food.price;
        orderItem.total = orderItem.price * orderItem.amount;
        orderItem.order = order;

        await this.orderItemsRepository.save(orderItem);
      }
      return updatedOrder;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.ordersRepository.softRemove(order);
  }
}
